cli version 3.20

{% if XX_timezone_offset -%}
clock local default-offset {{ XX_timezone_offset }}
{%- endif %}
{% if XX_dst_offset -%}
clock local dst-rule DSTRULE {{ XX_dst_offset }} from {{ XX_dst_start }} until {{ XX_dst_end }}
{%- endif %}

timer PROVISIONING_CONFIG now + 1 minute "provisioning execute PF_PROVISIONING_CONFIG"
timer PROVISIONING_FIRMWARE now + 1 minute "provisioning execute PF_PROVISIONING_FIRMWARE"

{% if ntp_enabled -%}
sntp-client
sntp-client server primary {{ ntp_ip }}
{%- endif %}

{% if admin_password -%}
administrator {{ admin_username|d('administrator') }} password {{ admin_password }}
{%- endif %}
{% if user_username and user_password -%}
operator {{ user_username }} password {{ user_password }}
{%- endif %}

profile provisioning PF_PROVISIONING_CONFIG
  destination configuration
  location 1 $(dhcp.66)/$(system.mac).cfg
  activation reload graceful

actions
  rule ACT_CHECK_SYNC
    condition sip gateway:GW_SIP NOTIFY_CHECK_SYNC_RELOAD
    action "provisioning execute PF_PROVISIONING_CONFIG"

{% if syslog_enabled -%}
syslog-client
  remote {{ syslog_ip }} {{ syslog_port }}
    facility kernel severity informational
    facility sip severity {{ XX_syslog_level }}
{%- endif %}

system
  ic voice 0
    low-bitrate-codec g729

{% include 'region_FR.tpl' %}

context ip router
  interface eth0
    ipaddress dhcp
    tcp adjust-mss rx mtu
    tcp adjust-mss tx mtu

{% if vlan_enabled and vlan_priority is defined -%}
profile service-policy SP_VLAN_PRIO
  source traffic-class default
    set layer2 cos {{ vlan_priority }}

context ip router
  interface eth0
    use profile service-policy SP_VLAN_PRIO out
{%- endif %}

port ethernet 0 0
  medium auto
  {% if vlan_enabled -%}
  vlan {{ vlan_id }}
    encapsulation ip
    bind interface eth0 router
    no shutdown
  {%- else -%}
  encapsulation ip
  bind interface eth0 router
  {%- endif %}

port ethernet 0 0
  no shutdown

context cs switch
  routing-table called-e164 FROM-ISDN-TO-XIVO
  {% for server in XX_servers -%}
    route .T dest-interface IF_SIP_SERVER{{ server['id'] }}
  {% endfor %}

  interface isdn T0-PORT-0
    route call dest-table FROM-ISDN-TO-XIVO

  interface isdn T0-PORT-1
    route call dest-table FROM-ISDN-TO-XIVO

  {% for server in XX_servers -%}
  interface sip IF_SIP_SERVER{{ server['id'] }}
    bind context sip-gateway GW_SIP
    route call dest-service CHECK_HANGUP
    remote {{ server['proxy_ip'] }} {{ server['proxy_port'] }}
  {% endfor %}

  service hunt-group CHECK_HANGUP
    drop-cause normal-unspecified
    drop-cause no-circuit-channel-available
    drop-cause network-out-of-order
    drop-cause temporary-failure
    drop-cause switching-equipment-congestion
    drop-cause access-info-discarded
    drop-cause circuit-channel-not-available
    drop-cause resources-unavailable
    drop-cause user-busy
    route call 1 dest-interface T0-PORT-0
    route call 2 dest-interface T0-PORT-1


context cs switch
  no shutdown

authentication-service AUTH_TRK_XIVO
  username smartnode4120 password 1n/dpyfZmQqEjghRP7uFsg== encrypted

{% for server in XX_servers -%}
location-service LOC_SERVER{{ server['id'] }}
  domain {{ server['proxy_ip'] }}

  {% for line in server['lines'] -%}
  match-any-domain
  identity smartnode4120
    authentication inbound
      authenticate 1 authentication-service AUTH_TRK_XIVO username smartnode4120
  
    registration inbound
  {% endfor %}
{% endfor %}

context sip-gateway GW_SIP
  interface TRANSPORT_ETH0
    bind interface eth0 port 5060

context sip-gateway GW_SIP
  {% for server in XX_servers -%}
  bind location-service LOC_SERVER{{ server['id'] }}
  {% endfor %}
  notify check-sync accept
  no shutdown

port bri 0 0
  clock slave
  encapsulation q921
  q921
    protocol pp
    uni-side user
    encapsulation q931
    q931
      protocol dss1
      uni-side user
      bchan-number-order ascending
      encapsulation cc-isdn
      bind interface T0-PORT-0 switch

port bri 0 0
no shutdown

port bri 0 1
  clock slave
  encapsulation q921
  q921
    protocol pp
    uni-side user
    encapsulation q931
    q931
      protocol dss1
      uni-side user
      bchan-number-order ascending
      encapsulation cc-isdn
      bind interface T0-PORT-1 switch

port bri 0 1
no shutdown
