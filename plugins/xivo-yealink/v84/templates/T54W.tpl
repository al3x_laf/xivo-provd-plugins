{% extends 'base.tpl' -%}

{% block model_specific_parameters -%}
gui_lang.delete = http://localhost/003.GUI.French.lang
programablekey.3.type = 0

phone_setting.backgrounds = xivologo.png
phone_setting.idle_dsskey_and_title.transparency = 20%
wallpaper_upload.url = http://{{ ip }}:8667/xivologo.png

phone_setting.idle_clock_display.enable=0
{% endblock %}
