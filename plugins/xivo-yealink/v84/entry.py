# -*- coding: utf-8 -*-

# Copyright (C) 2013-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os

common_globals = {}
execfile_('common.py', common_globals)

MODEL_VERSIONS = {
    u'T19P_E2': u'53.84.0.15',
    u'T21P_E2': u'52.84.0.15',
    u'T23P': u'44.84.0.15',
    u'T23G': u'44.84.0.15',
    u'T27G': u'69.84.0.15',
    u'T40P': u'54.84.0.15',
    u'T40G': u'76.84.0.15',
    u'T41S': u'66.84.0.15',
    u'T42S': u'66.84.0.15',
    u'T46S': u'66.84.0.15',
    u'T48S': u'66.84.0.15',
    u'T54W': u'96.84.0.30',
}
COMMON_FILES = [
    ('y000000000052.cfg', u'T21P_E2-52.84.0.15.rom', 'model.tpl'),
    ('y000000000053.cfg', u'T19P_E2-53.84.0.15.rom', 'model.tpl'),
    ('y000000000044.cfg', u'T23-44.84.0.15.rom', 'model.tpl'),
    ('y000000000069.cfg', u'T27G-69.84.0.15.rom', 'model.tpl'),
    ('y000000000054.cfg', u'T40-54.84.0.15.rom', 'model.tpl'),
    ('y000000000076.cfg', u'T40G-76.84.0.15.rom', 'model.tpl'),
    ('y000000000065.cfg', u'T46S(T48S,T42S,T41S)-66.84.0.15.rom', 'model.tpl'), # T48S
    ('y000000000066.cfg', u'T46S(T48S,T42S,T41S)-66.84.0.15.rom', 'model.tpl'), # T46S
    ('y000000000067.cfg', u'T46S(T48S,T42S,T41S)-66.84.0.15.rom', 'model.tpl'), # T42S
    ('y000000000068.cfg', u'T46S(T48S,T42S,T41S)-66.84.0.15.rom', 'model.tpl'), # T41S
    ('y000000000096.cfg', u'T54W-96.84.0.30.rom', 'model.tpl'),
]


class YealinkPlugin(common_globals['BaseYealinkPlugin']):
    IS_PLUGIN = True

    pg_associator = common_globals['BaseYealinkPgAssociator'](MODEL_VERSIONS)

    # Yealink plugin specific stuff

    _COMMON_FILES = COMMON_FILES
